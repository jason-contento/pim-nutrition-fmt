function fixPercent(context){
	let n = parseInt(String(context).replace(/[^0-9\.\-]/g,'') || 0)
	if(isNaN(n)) n = 0
    return n
}

function fixMass(context) {
	if(!context) context = ''
	str = String(context).toLowerCase()
	.replace(/ gr/i,'g')
	.replace(/ gm/i,'g')
	.replace(/ mg/i,'mg')
	.replace(/ g/i,'g')
	return str
}

function fixInteger(context) {
	return parseInt(String(context).replace(/[^0-9\.\-]/g,'') || 0)
}

function fixIngredients(context) {
	var str = context ? String(context) : ''
	return str.replace(/^ingredients:/gi,'').replace(/^\s+|\s+$/g,'')
}

const mapProcesses = {
	ID:function(entry){
		return entry.GTIN
	},
	GTIN:function(entry){
		return entry.GTIN
	},
	UPC:function(entry){
		return entry.UPC
	},
	Name:function(entry){
		return entry.eCommProductName
	},
	Description:function(entry){
		return entry.eCommProductFeature || entry.GeneralMktDescription || entry.eCommProductFeature
	},
	nutrition_allergens:function(entry){
		if(entry.ProductAllergens && entry.ProductAllergens.length>0){
			return entry.ProductAllergens.map(function(a){
				return a.Allergen
			}).join(', ')
		}
		return ''
	},
	nutrition_servings:function(entry){
		if(entry.ServingsOfTradeItemUnit){
			if(parseFloat(entry.ServingsOfTradeItemUnit)>1){
				return 'About '+entry.ServingsOfTradeItemUnit+' servings per container'
			} else {
				return 'About '+entry.ServingsOfTradeItemUnit+' servings per container'
			}
		}
		return '1 serving per container'
	},
	nutrition_servingsize:function(entry){
		return entry.IngredientInfoHHServingSize || entry.NutrSrv1ServSize || ''
	},
	nutrition_calories:function(entry){
		return fixInteger(entry.NutrSrv1CalAmt || 0)
	},
	nutrition_caloriesfromfat:function(entry){
		return fixInteger(entry.NutrSrv1EnerpfAmt || 0)
	},
	nutrition_vitamina_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_VITAA_RDA || entry.NutrSrv1VitARDA || 0)
	},
	nutrition_vitaminc_dv:function(entry){
		return fixPercent(entry.NutrSrv1VitCRDA || 0)
	},
	nutrition_calcium_dv:function(entry){
		return fixPercent(entry.NutrSrv1CaRDA || 0)
	},
	nutrition_iron_dv:function(entry){
		return fixPercent(entry.NutrSrv1HaemRDA || 0)
	},
	nutrition_vitamind_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_VITD_RDA || 0)
	},
	nutrition_vitamine_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_VITE_RDA || 0)
	},
	nutrition_riboflavin_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_RIBF_RDA || 0)
	},
	nutrition_folate_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_FOL_RDA || 0)
	},
	nutrition_zinc_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_ZN_RDA || 0)
	},
	nutrition_selenium_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_SE_RDA || 0)
	},
	nutrition_copper_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_CU_RDA || 0)
	},
	nutrition_chromium_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_CR_RDA || 0)
	},
	nutrition_molybdenum_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_MO_RDA || 0)
	},
	nutrition_chloride_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_CLD_RDA || 0)
	},
	nutrition_iodine_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_ID_RDA || 0)
	},
	nutrition_biotin_dv:function(entry){
		return fixMass(entry.F_NUTRSRV1_BIOT_RDA || 0)
	},
	nutrition_vitaminb12_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_VITB12_RDA || 0)
	},
	nutrition_vitamink_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_VITK_RDA || 0)
	},
	nutrition_pantothenicacid_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_PANTAC_RDA || 0)
	},
	nutrition_thiamin_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV2_THIA_RDA || 0)
	},
	nutrition_niacin_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_NIA_RDA || 0)
	},
	nutrition_phosphorus_dv:function(entry){
		return fixPercent(entry.NutrSrv1PRDA || 0)
	},
	nutrition_magnesium_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_MG_RDA || 0)
	},
	nutrition_manganese_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_MN_RDA || 0)
	},
	nutrition_fat_amt:function(entry){
		return fixMass(entry.NutrSrv1FatAmt || 0)
	},
	nutrition_fat_dv:function(entry){
		return fixPercent(entry.NutrSrv1FatRDA || 0)
	},
	nutrition_saturatedfat_amt:function(entry){
		return fixMass(entry.NutrSrv1FastatAmt || 0)
	},
	nutrition_saturatedfat_dv:function(entry){
		return fixPercent(entry.NutrSrv1FastatRDA || 0)
	},
	nutrition_transfat_amt:function(entry){
		return fixMass(entry.NutrSrv1FatrnAmt || 0)
	},
	nutrition_polyunsaturatedfat_amt:function(entry){
		return fixMass(entry.F_NUTRSRV1_FAPU_AMT || 0)
	},
	nutrition_monounsaturatedfat_amt:function(entry){
		return fixMass(entry.F_NUTRSRV1_FAMS_AMT || 0)
	},
	nutrition_cholesterol_amt:function(entry){
		return fixMass(entry.F_NUTRSRV1_CHOL_AMT || 0)
	},
	nutrition_cholesterol_dv:function(entry){
		return fixPercent(entry.NutrSrv1CholRDA || 0)
	},
	nutrition_sodium_amt:function(entry){
		return fixMass(entry.NutrSrv1NaAmt || 0)
	},
	nutrition_sodium_dv:function(entry){
		return fixPercent(entry.NutrSrv1NaRDA || 0)
	},
	nutrition_potassium_amt:function(entry){
		return fixMass(entry.F_NUTRSRV1_K_AMT || 0)
	},
	nutrition_potassium_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_K_RDA || 0)
	},
	nutrition_carbohydrates_amt:function(entry){
		return fixMass(entry.F_NUTRSRV1_CHO_AMT || 0)
	},
	nutrition_carbohydrates_dv:function(entry){
		return fixPercent(entry.NutrSrv1ChoRDA || 0)
	},
	nutrition_fiber_amt:function(entry){
		return fixMass(entry.NutrSrv1FibtswAmt || 0)
	},
	nutrition_fiber_dv:function(entry){
		return fixPercent(entry.NutrSrv1FibtswRDA || 0)
	},
	nutrition_sugars_amt:function(entry){
		return fixMass(entry.NutrSrv1SugarAmt || 0)
	},
	nutrition_protein_amt:function(entry){
		return fixMass(entry.NutrSrv1ProAmt || 0)
	},
	nutrition_protein_dv:function(entry){
		return fixPercent(entry.F_NUTRSRV1_PRO_RDA || 0)
	},
	nutrition_contains:function(entry){
		return entry.IngredientInfoDoesContainStatement || ''
	},
	nutrition_ingredients:function(entry){
		return entry.IngredientInformationStatement || ''
	},
	BrandName:function(entry){
		if(entry.ProductBrands && entry.ProductBrands.length>0){
			return entry.ProductBrands[0].Name
		}
	},
	BrandID:function(entry){
		if(entry.ProductBrands && entry.ProductBrands.length>0){
			return entry.ProductBrands[0].BrandID
		}
	},
	alt_image:function(entry){

		let images = []
		let imageData = {}

		entry.ProductAssets
		.filter(function(productAsset){
			return productAsset.UserTypeID==="ECOMMERCE_IMAGES" && productAsset.Renditions
		})
		.map(function(productAsset){
			productAsset.Renditions.map(function(rendition){
				// fixed broken path

				//if(rendition.AssetPath.indexOf('.JPG.jpg')>-1){
					//console.log('IMage path problems:',entry.GTIN)
				//}
				rendition.AssetPath = rendition.AssetPath.replace('.JPG.jpg','.jpg')

				// take width/height from name to check against actualy image size
				let d = rendition.RenditionName.split('x')
				let imageWidth = parseInt(d[0])
				let imageHeight = parseInt(d[1])
				let imageType = rendition.AssetPath.split('_').pop().split('.')[0]

				if(!imageData[imageType]){
					imageData[imageType] = []
				}

				imageData[imageType].push({
					url:rendition.AssetPath,
					width:imageWidth,
					height:imageHeight
				})
			})
		})

		for(let e in imageData){
			imageData[e].sort(function(a,b){
				let _a = a.width*a.height
				let _b = b.width*b.height
				return _a<_b ? -1 : _a>_b ? 1 : 0
			})

			images.push(imageData[e].pop().url)
		}

		return JSON.stringify(images)
	}
}

function convertProduct(entry){
	let result = {properties:[]}
	for(var e in mapProcesses){
		let val = mapProcesses[e](entry)
		if(val){
			// put properties in PIM format to mirror what the microservice will export
			result.properties.push({
				name:e,
				values:[val]
			})
		}
	}
	return result
}

module.exports = {
	convertProduct:convertProduct
}