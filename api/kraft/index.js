var ApplePie = require('../ApplePie')
var Cacher = require('../CacheResponse.js')

var KraftBrands = new ApplePie({
	url:'http://api.kraftapps.com/v4/ecomm/api/brands/',
	defaultResponse:{
		error:1
	}
})

var KraftBrandProducts = new ApplePie({
	url:'http://api.kraftapps.com/v4/ecomm/api/products/filter/',
	get:{
    	ccode:'USA',
    	lcode:'en',
    	sord:'asc',
    	sidx:'GTIN',
    	bid:0,
    	startNum:1,
    	endNum:500
    },
	defaultResponse:{
		error:1
	}
})

var KraftProducts = new ApplePie({
	url:'http://api.kraftapps.com/v4/ecomm/api/products/gtin/eng',
	method:'POST',
	get:{
		sort:'Ascending',
		orderBy:'eCommProductName',
		pageSize:100,
		pageNumber:1
	},
	post:{
		list:{
			process:function(data, query){
				if(data instanceof Array){
					return data.join(',')
				}
				return data
			}
		}
	},
	defaultResponse:{
		error:1
	}
})

var KraftRecipe = new ApplePie({
	url:'http://api.kraftapps.com/v4/Recipes',
	headers : {
        "Authorization" : "Basic a3JhZnQ6a3JhZnQ="
    },
	get:{
    	recipeIds:{
			process:function(data, query){
				if(data instanceof Array){
					return data.join(',')
				}
				return data
			}
		}
    },
	defaultResponse:{
		error:1
	}
})

var KraftProduct = new ApplePie({
	url:'http://api.kraftapps.com/v4/ecomm/api/products/gtin/eng',
	rest:['productID']
})

function getRecipes(ids, cb){
	// need a way to set a cache key based on the list of ids
	// so sum them and count them - this should make the names unique most of the time
	// not bullet proof for sure but this is just the cache
	let arr = ids.toString().split(',')
	let sum = arr.reduce(function(n,id){
		return n + parseInt(id)
	},0)
	let cacheKey = 'getRecipes_'+arr.length+'_'+sum

	if(Cacher.isCached(cacheKey)){
		cb(Cacher.getCache(cacheKey))
	} else {
		KraftRecipe.send({
			get:{
				recipeIds:ids
			}
		}, function(data){
			Cacher.setCache(cacheKey,JSON.parse(data))
			cb(data)
		})
	}
}

function getProducts(ids,cb){
	let cacheKey = 'getProducts_'+ids.toString()
	if(Cacher.isCached(cacheKey)){
		cb(Cacher.getCache(cacheKey))
	} else {
		KraftProducts.send({
			post:{
				list:ids
			}
		}, function(data){
			Cacher.setCache(cacheKey,JSON.parse(data))
			cb(data)
		})
	}
}

function getProduct(id,cb){
	let cacheKey = 'getProduct_'+id
	if(Cacher.isCached(cacheKey)){
		//console.log('pull cache for',id)
		cb(Cacher.getCache(cacheKey))
	} else {
		console.log('pull API for',id)
		KraftProduct.send({
			rest:{
				productID:id
			}
		}, function(data){
			Cacher.setCache(cacheKey,JSON.parse(data))
			cb(data)
		})
	}
}

function getBrands(cb){
	KraftBrands.send({}, cb)
}

/*

START: HYDRATED/OPTIMIZED BRANDS AND PRODUCTS REQUEST

*/
function loadBrandProducts(brands,response,cb){
	if(brands.length>0){
		let brand = brands.pop()
		getBrandProducts(brand.BrandID, function(productsJSON){
			brand.products = JSON.parse(productsJSON).Products || []
			response.Brands.push(brand)
			loadBrandProducts(brands,response,cb)
		})
	} else {
		cb(response)
	}
}

function cleanBrandProducts(data){
	return {
		Brands:data.Brands
		.filter(function(brand){
			return brand.products.length>0
		})
		.map(function(brand){
			return {
				ID:brand.BrandID,
				Name:brand.Name,
				Products:brand.products.map(function(product){
					return {
						ID:product.GTIN,
						Name:product.Name
					}
				})
			}
		})
	}
}

function getBrandsWithProducts(cb){
	let cacheKey = 'getBrandsWithProducts'
	if(Cacher.isCached(cacheKey,1000*60*60*24*3)){
		cb(Cacher.getCache(cacheKey))
	} else {
		getBrands(function(data){
			let brands = JSON.parse(data)
			let response = Object.assign({},brands,{Brands:[]})
			loadBrandProducts(brands.Brands,response,function(hydratedBrands){
				hydratedBrands = cleanBrandProducts(hydratedBrands)
				Cacher.setCache(cacheKey,hydratedBrands)
				cb(JSON.stringify(hydratedBrands))
			})
		})
	}
}
/*

END: HYDRATED/OPTIMIZED BRANDS AND PRODUCTS REQUEST

*/

function getBrandProducts(brandId, cb){
	let cacheKey = 'getBrandProducts_'+brandId
	if(Cacher.isCached(cacheKey)){
		cb(Cacher.getCache(cacheKey))
	} else {
		KraftBrandProducts.send({
			get:{
				bid:brandId
			}
		}, function(data){
			Cacher.setCache(cacheKey,JSON.parse(data))
			cb(data)
		})
	}
}

function getAllProducts(cb){
	getBrands(function(brands){
		brands = JSON.parse(brands).Brands.map(function(brand){
			return {
				ID:brand.BrandID,
				products:[],
				name:brand.Name
			}
		})

		console.log('Loading',brands.length,'Brands')
		let loadedBrands = brands.length
		
		brands.map(function(brand){
			getBrandProducts(brand.ID, function(products){
				console.log('Loaded',brand.name)
				brand.products = JSON.parse(products).Products
				loadedBrands--
				if(loadedBrands===0){
					brands = brands.filter(function(brand){
						return brand.products.length>0
					})
					cb(brands)
				}
			})
		})
	})
}

module.exports = {
	getBrandsWithProducts:getBrandsWithProducts,
	getRecipes:getRecipes,
	getProduct:getProduct,
	getProducts:getProducts,
	getBrands:getBrands,
	getBrandProducts:getBrandProducts,
	getAllProducts:getAllProducts
}