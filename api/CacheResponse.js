var fs = require('fs')
var path = require('path')

const EXPIRE_AT = 1000*60*60*24*3

function getPath(filename){
	return path.join(__dirname,'./cache/'+filename+'.json')
}

function saveFile(filename,data){
	let filepath = getPath(filename)
	if(typeof data==='string'){
		let content = JSON.parse(data)
	}
	let content = JSON.stringify(data,null,2)
	fs.writeFileSync(filepath, content, {flag:'w'})
}

function getFile(filename){
	let filepath = getPath(filename)
	if(fs.existsSync(filepath)){
		let content = fs.readFileSync(filepath, 'utf8')
		return content
	} else {
		return false
	}
}

function isCached(filename, maxAge){
	let filepath = getPath(filename)
	let exists = fs.existsSync(filepath)
	if(exists){
		let now = new Date().getTime()
		let ctime = fs.statSync(filepath).ctime
		let createdAt = new Date(ctime).getTime() + (maxAge || EXPIRE_AT)
		return now<(createdAt+EXPIRE_AT)
	}
	return exists
}

function getCache(key){
	return getFile(key)
}

function setCache(key,data){
	return saveFile(key,data)
}

module.exports = {
	isCached:isCached,
	getCache:getCache,
	setCache:setCache
}