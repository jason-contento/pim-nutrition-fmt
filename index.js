const express = require('express')
const fs = require('fs')
const path = require('path')
const Freemarker = require('./custom_modules/freemarker.js/')
const bodyParser = require('body-parser')
const openport = require('openport')
const opn = require('opn')
const KraftAPI = require('./api/kraft/')
const remap = require('./utils/remapKraftProduct.js')

const app = express()
const fm = new Freemarker({
	viewRoot: __dirname+'/freemarker',
	options: {}
})

app.use(express.static(__dirname + '/public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/api/kraft/product/:productId', function(req, res) {
	KraftAPI.getProduct(req.params.productId, function(productsResp){
		res.end(productsResp)
	})
})

app.get('/api/kraft/brands', function(req, res) {
	KraftAPI.getBrandsWithProducts(function(jsonResponse){
		res.end(jsonResponse)
	})
})

app.get('/test', function(req, res) {
	// use static example of export format in data folder
	let content = fs.readFileSync(path.join(__dirname,'./data/KraftPIMProducts.json'), 'utf8')
	let data = JSON.parse(content).products[0]
	KraftAPI.getBrandsWithProducts(function(jsonResponse){
		data.Brands = JSON.parse(jsonResponse).Brands
		fm.render('framework.html', data, function(err, html, output) {
			res.end(html)
		})
	})
})

app.get('/:productId', function(req, res) {
	KraftAPI.getProduct(req.params.productId, function(productsResp){
		let data = remap.convertProduct(JSON.parse(productsResp))
		KraftAPI.getBrandsWithProducts(function(jsonResponse){
			data.Brands = JSON.parse(jsonResponse).Brands
			fm.render('framework.html', data, function(err, html, output) {
				if(err){
					console.log(err)
				}
				res.end(html)
			})
		})
	})
})

openport.find({startingPort:5000}, function(err, port) {
    if (err) {
        console.log(err)
    } else {
        app.listen(port, function() {
            console.log('Running on port', port)
            opn(`http://localhost:${port}/00043000038819`, { app: ['google chrome'] })
        })
    }
})